﻿using System;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

using SharpVectors.Runtime;
using SharpVectors.Renderers;
using SharpVectors.Renderers.Wpf;
using SharpVectors.Converters;

namespace SvgViewerCs
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {

        class PointContiner
        {
            public enum PointTypes
            {
                Translation,
                RotationRoll,
            }
            public PointTypes PointType { get; set; }
            public Point Value { get; set; }
        }

        PointContiner mousePositionBuf = null;

        public MainWindow()
        {
            InitializeComponent();
        }


        #region[methods]
        private void fixStateStyle()
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                BorderThickness = new Thickness(6);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Visible;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                BorderThickness = new Thickness(0);
                this.buttonNormalize.Visibility = System.Windows.Visibility.Collapsed;
                this.buttonMaximize.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void OpenSvgUseStream(string path)
        {
            if (IsSvgz(path))
            {
                Message(string.Format("[MainWindow.OpenSvgUseTempFile()] decompressed stream"));
                using (FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (GZipStream zStream = new GZipStream(fStream, CompressionMode.Decompress))
                {
                    OpenSvgFile(delegate(FileSvgReader fileReader)
                    {
                        return fileReader.Read(zStream);
                    });
                }
            }
            else
            {
                Message(string.Format("[MainWindow.OpenSvgUseTempFile()] plain stream"));
                using (FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    OpenSvgFile(delegate(FileSvgReader fileReader)
                    {
                        return fileReader.Read(fStream);
                    });
                }
            }
        }

        private void OpenSvgUseTempFile(string path)
        {
            string tmpPath = System.IO.Path.GetTempFileName();
            Message(string.Format("[MainWindow.OpenSvgUseTempFile()] tmp({0})", new object[] { tmpPath }));
            if (IsSvgz(path))
            {
                Message(string.Format("[MainWindow.OpenSvgUseTempFile()] decompressed copy to tmp"));
                using (FileStream oStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (GZipStream rStream = new GZipStream(oStream, CompressionMode.Decompress))
                using (FileStream wStream = new FileStream(tmpPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    byte[] buf = new byte[1024];
                    int count;
                    while ((count = rStream.Read(buf, 0, buf.Length)) > 0)
                    {
                        wStream.Write(buf, 0, count);
                    }
                }
            }
            else
            {
                Message(string.Format("[MainWindow.OpenSvgUseTempFile()] plain copy to tmp"));
                using (FileStream rStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (FileStream wStream = new FileStream(tmpPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    byte[] buf = new byte[1024];
                    int count;
                    while ((count = rStream.Read(buf, 0, buf.Length)) > 0)
                    {
                        wStream.Write(buf, 0, count);
                    }
                }
            }
            OpenSvgFile(delegate(FileSvgReader fileReader)
            {
                return fileReader.Read(tmpPath);
            });
        }

        private void OpenSvgFile(Func<FileSvgReader, DrawingGroup> read)
        {
            WpfDrawingSettings wpfSettings;
            FileSvgReader fileReader;
            DrawingGroup drwing;

            mousePositionBuf = null;
            matrixTransform.Matrix = Matrix.Identity;
            svgDrawingCanvas.UnloadDiagrams();

            wpfSettings = new WpfDrawingSettings();
            wpfSettings.CultureInfo = wpfSettings.NeutralCultureInfo;
            fileReader = new FileSvgReader(wpfSettings);
            //drwing = fileReader.Read(path);
            drwing = read(fileReader);
            if (drwing != null)
            {
                svgDrawingCanvas.RenderDiagrams(drwing);
            }
        }
        private static bool IsSvgz(string path)
        {
            try
            {
                using (FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    bool ret = true;
                    ret &= fStream.ReadByte() == 0x1F;
                    ret &= fStream.ReadByte() == 0x8B;
                    return ret;
                }
            }
            catch
            {
                return false;
            }
        }
        private void OpenSvgStreamFromPath(string path, bool isSvgz = false)
        {
            if (isSvgz)
            {
                using (FileStream fStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                using (GZipStream gStream = new GZipStream(fStream, CompressionMode.Decompress))
                {
                    OpenSvgStream(gStream);
                }
            }
            else
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    OpenSvgStream(stream);
                }
            }
        }

        private void OpenSvgStream(Stream stream)
        {
            WpfDrawingSettings wpfSettings;
            FileSvgReader fileReader;
            DrawingGroup drwing;

            mousePositionBuf = null;
            matrixTransform.Matrix = Matrix.Identity;
            svgDrawingCanvas.UnloadDiagrams();

            wpfSettings = new WpfDrawingSettings();
            wpfSettings.CultureInfo = wpfSettings.NeutralCultureInfo;
            fileReader = new FileSvgReader(wpfSettings);
            drwing = fileReader.Read(stream);
            if (drwing != null)
            {
                svgDrawingCanvas.RenderDiagrams(drwing);
            }
        }

        private void Message(string s)
        {
            const int MAX_LINE = 100;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("[{0}] {1}", DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"), s));
            using (StringReader r = new StringReader(textBox.Text))
            {
                for (int i = 0; i < MAX_LINE - 1; i++)
                {
                    sb.AppendLine(r.ReadLine());
                }
            }
            textBox.Text = sb.ToString();
        }
        #endregion

        #region [event]
        private void window_Initialized(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        private void window_StateChanged(object sender, EventArgs e)
        {
            fixStateStyle();
        }
        #endregion

        #region [command]
        private void CommandCloseCommand(object sender, RoutedEventArgs e)
        {
            SystemCommands.CloseWindow(this);
        }
        private void CommandMaximizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }
        private void CommandMinimizeWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }
        private void CommandRestoreWindow(object sender, RoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }
        #endregion

        #region [menu]
        private void FileMenuOpen(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "all file|*|svg(svg, svgz)|*.svg;*.svgz";
            dialog.FilterIndex = 2;
            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;
                try
                {
                    OpenSvgUseStream(path);
                    Message(string.Format("[MainWindow.FileMenuOpen()] loaded({0})", new object[] { path }));
                }
                catch (Exception ex)
                {
                    Message(string.Format("[MainWindow.FileMenuOpen()] {0}", new object[] { ex.ToString() }));
                    Message(string.Format("[MainWindow.FileMenuOpen()] faild...({0})", new object[] { path }));
                }
            }
        }
        private void FileMenuOpenUseTempFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "all file|*|svg(svg, svgz)|*.svg;*.svgz";
            dialog.FilterIndex = 2;
            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;
                try
                {
                    OpenSvgUseTempFile(path);
                    Message(string.Format("[MainWindow.FileMenuOpenUseTempFile()] loaded({0})", new object[] { path }));
                }
                catch (Exception ex)
                {
                    Message(string.Format("[MainWindow.FileMenuOpenUseTempFile()] {0}", new object[] { ex.ToString() }));
                    Message(string.Format("[MainWindow.FileMenuOpenUseTempFile()] faild...({0})", new object[] { path }));
                }
            }
        }
        private void FileMenuOpenDirect(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "all file|*|svg(svg, svgz)|*.svg;*.svgz";
            dialog.FilterIndex = 2;
            if (dialog.ShowDialog() == true)
            {
                string path = dialog.FileName;
                try
                {
                    OpenSvgFile(delegate(FileSvgReader fileReader)
                    {
                        return fileReader.Read(path);
                    });

                    Message(string.Format("[MainWindow.FileMenuOpenDirect()] loaded({0})", new object[] { path }));
                }
                catch (Exception ex)
                {
                    Message(string.Format("[MainWindow.FileMenuOpenDirect()] {0}", new object[] { ex.ToString() }));
                    Message(string.Format("[MainWindow.FileMenuOpenDirect()] faild...({0})", new object[] { path }));
                }
            }
        }
        private void WindowMenuTopMost(object sender, RoutedEventArgs e)
        {
            this.Topmost = !this.Topmost;
            menuItemTopMost.IsChecked = this.Topmost;
        }
        private void HelpMenuVersion(object sender, RoutedEventArgs e)
        {
        }
        #endregion

        #region [control]
        private void MouseEventZoomWheel(object sender, MouseWheelEventArgs e)
        {
            double s = (e.Delta / (120.0 * 3.0));
            double scale;
            //calc scale
            {
                if (s > 0)
                {
                    scale = s;
                }
                else if (s < 0)
                {
                    scale = 1 / Math.Abs(s);
                }
                else
                {
                    scale = 1;
                }

            }
            //calc matrix
            {
                Matrix m = matrixTransform.Matrix;
                Matrix m1 = Matrix.Identity;
                m1.Scale(scale, scale);
                m = Matrix.Multiply(m, m1);
                matrixTransform.Matrix = m;
            }
            Message(string.Format("[SvgViewer.MouseEventZoomWheel()] delta({0}) s({1}({2}))", new object[] { e.Delta, s, scale }));
        }

        private void MouseEventStart(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                mousePositionBuf = new PointContiner() { PointType = PointContiner.PointTypes.Translation, Value = e.GetPosition(this) };
                Mouse.OverrideCursor = Cursors.ScrollAll;
                Message(string.Format("[SvgViewer.MouseEventStart()] mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                mousePositionBuf = new PointContiner() { PointType = PointContiner.PointTypes.RotationRoll, Value = e.GetPosition(this) };
                Mouse.OverrideCursor = Cursors.SizeWE;
                Message(string.Format("[SvgViewer.MouseEventStart()] mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
            }
        }

        private void MouseEventMoving(object sender, MouseEventArgs e)
        {
            if (mousePositionBuf != null)
            {
                Point pos = e.GetPosition(this);
                switch (mousePositionBuf.PointType)
                {
                    case PointContiner.PointTypes.Translation:
                        {
                            Point p = new Point(pos.X - mousePositionBuf.Value.X, pos.Y - mousePositionBuf.Value.Y);
                            //calc matrix
                            {
                                Matrix m = matrixTransform.Matrix;
                                TranslateTransform t = new TranslateTransform();
                                t.X = p.X;
                                t.Y = p.Y;
                                m = Matrix.Multiply(m, t.Value);
                                matrixTransform.Matrix = m;
                            }
                            mousePositionBuf.Value = pos;
                            Message(string.Format("[SvgViewer.MouseEventMoving()] TR mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
                            break;
                        }
                    case PointContiner.PointTypes.RotationRoll:
                        {
                            double roll = (pos.X - mousePositionBuf.Value.X);
                            //calc matrix
                            {
                                Matrix m = matrixTransform.Matrix;
                                RotateTransform t = new RotateTransform();
                                t.Angle = roll;
                                m = Matrix.Multiply(m, t.Value);
                                matrixTransform.Matrix = m;
                            }
                            mousePositionBuf.Value = pos;
                            Message(string.Format("[SvgViewer.MouseEventMoving()] RL mousePositionBuf({0}, {1})", new object[] { mousePositionBuf.Value.X, mousePositionBuf.Value.Y }));
                            break;
                        }
                    default:
                        {
                            mousePositionBuf = null;
                            break;
                        }
                }
            }
        }

        private void MouseEventEnd(object sender, MouseButtonEventArgs e)
        {
            mousePositionBuf = null;
            Mouse.OverrideCursor = null;
            Message(string.Format("[SvgViewer.MouseEventEnd()]"));
        }
        #endregion

        private void MouseEventDragEnter(object sender, DragEventArgs e)
        {
        }

        private void MouseEventDragDrop(object sender, DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                string file = files[0];
                OpenSvgUseTempFile(file);
                Message(string.Format("[MainWindow.MouseEventDragDrop()] succeed"));
            }
            catch (Exception ex)
            {
                Message(string.Format("[MainWindow.MouseEventDragDrop()] failed... {0}", new object[] { ex.ToString() }));
            }
        }
    }
}
